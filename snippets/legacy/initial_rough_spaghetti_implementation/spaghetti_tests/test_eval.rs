mod eval_tests {
    use relish::ast::{ast_to_string, eval, lex, new_ast, FTable, VTable};
    use relish::ast::{func_declare, Args};
    use relish::ast::{Ctr, ExternalOperation, Function, Operation};
    use std::cell::RefCell;
    use std::rc::Rc;

    // TODO: write generalized testing routine on top of list of inputs

    #[test]
    fn eval_singlet() {
        let test_doc = "(1)".to_string();
        let ft = Rc::new(RefCell::new(FTable::new()));
        let vt = Rc::new(RefCell::new(VTable::new()));

        match lex(test_doc.clone()) {
            Err(e) => {
                println!("Lexing error: {}\n", e);
                assert!(false)
            }

            Ok(initial_ast) => match eval(initial_ast.clone(), vt.clone(), ft.clone(), false) {
                Err(e) => {
                    println!("Evaluation error: {}\n", e);
                    assert!(false)
                }

                Ok(reduced) => {
                    if let Ctr::Seg(reduced_ast) = reduced {
                        assert_eq!(ast_to_string(reduced_ast), test_doc)
                    }
                }
            },
        }
    }

    #[test]
    fn eval_embedded_lists_no_funcs() {
        let test_doc = "(1 (1 2 3 4 5) 5)".to_string();
        let ft = Rc::new(RefCell::new(FTable::new()));
        let vt = Rc::new(RefCell::new(VTable::new()));

        match lex(test_doc.clone()) {
            Err(e) => {
                println!("Lexing error: {}\n", e);
                assert!(false)
            }

            Ok(initial_ast) => match eval(initial_ast.clone(), vt.clone(), ft.clone(), false) {
                Err(e) => {
                    println!("Evaluation error: {}\n", e);
                    assert!(false)
                }

                Ok(reduced) => {
                    if let Ctr::Seg(reduced_ast) = reduced {
                        assert_eq!(ast_to_string(reduced_ast), test_doc)
                    }
                }
            },
        }
    }

    #[test]
    fn eval_function_call() {
        let test_doc = "('one' (echo 'unwrap_me'))".to_string();
        let output = "('one' 'unwrap_me')";
        let test_external_func: Function = Function {
            name: String::from("echo"),
            loose_syms: false,
            eval_lazy: false,
            args: Args::Lazy(1),
            function: Operation::External(ExternalOperation {
                arg_syms: vec!["input".to_string()],
                ast: new_ast(
                    Ctr::Seg(new_ast(Ctr::Symbol("input".to_string()), Ctr::None)),
                    Ctr::None,
                ),
            }),
        };

        let ft = Rc::new(RefCell::new(FTable::new()));
        let vt = Rc::new(RefCell::new(VTable::new()));
        if let Some(s) = func_declare(ft.clone(), Rc::new(RefCell::new(test_external_func))) {
            print!("Error declaring external func: {}", s);
            assert!(false);
        }

        match lex(test_doc) {
            Err(e) => {
                println!("Lexing error: {}\n", e);
                assert!(false)
            }

            Ok(initial_ast) => match eval(initial_ast.clone(), vt.clone(), ft.clone(), false) {
                Err(e) => {
                    println!("Evaluation error: {}\n", e);
                    assert!(false)
                }

                Ok(reduced) => {
                    if let Ctr::Seg(reduced_ast) = reduced {
                        let out_doc = ast_to_string(reduced_ast);
                        if out_doc != output {
                            print!("Erroneous output: {}\n", out_doc);
                            assert!(false)
                        }
                    }
                }
            },
        }
    }

    #[test]
    fn eval_embedded_func_calls() {
        let test_doc = "('one' (echo (echo 'unwrap_me')))".to_string();
        let output = "('one' 'unwrap_me')";
        let test_external_func: Function = Function {
            name: String::from("echo"),
            loose_syms: false,
            eval_lazy: false,
            args: Args::Lazy(1),
            function: Operation::External(ExternalOperation {
                arg_syms: vec!["input".to_string()],
                ast: new_ast(
                    Ctr::Seg(new_ast(Ctr::Symbol("input".to_string()), Ctr::None)),
                    Ctr::None,
                ),
            }),
        };

        let ft = Rc::new(RefCell::new(FTable::new()));
        let vt = Rc::new(RefCell::new(VTable::new()));
        if let Some(s) = func_declare(ft.clone(), Rc::new(RefCell::new(test_external_func))) {
            print!("Error declaring external func: {}", s);
            assert!(false);
        }

        match lex(test_doc) {
            Err(e) => {
                println!("Lexing error: {}\n", e);
                assert!(false)
            }

            Ok(initial_ast) => match eval(initial_ast.clone(), vt.clone(), ft.clone(), false) {
                Err(e) => {
                    println!("Evaluation error: {}\n", e);
                    assert!(false)
                }

                Ok(reduced) => {
                    if let Ctr::Seg(reduced_ast) = reduced {
                        let out_doc = ast_to_string(reduced_ast);
                        if out_doc != output {
                            print!("Erroneous output: {}\n", out_doc);
                            assert!(false)
                        }
                    }
                }
            },
        }
    }

    /*
    #[test]
    fn eval_bad_vars() {
        let test_doc = "".to_string();
        let ft = Rc::new(RefCell::new(FTable::new()));
        let vt = Rc::new(RefCell::new(VTable::new()));

        match lex(test_doc) {
            Err(e) => {
                println!("Lexing error: {}\n", e);
                assert!(false)
            },

            Ok(initial_ast) => {
                match eval(initial_ast.clone(), vt.clone(), ft.clone(), false) {
                    Err(e) => {
                        println!("Evaluation error: {}\n", e);
                        assert!(false)
                    },

                    Ok(reduced_ast) => {
                        // write tests here
                    }
                }
            }
        }
    }

    #[test]
    fn eval_bad_func() {
        let test_doc = "".to_string();
        let ft = Rc::new(RefCell::new(FTable::new()));
        let vt = Rc::new(RefCell::new(VTable::new()));

        match lex(test_doc) {
            Err(e) => {
                println!("Lexing error: {}\n", e);
                assert!(false)
            },

            Ok(initial_ast) => {
                match eval(initial_ast.clone(), vt.clone(), ft.clone(), false) {
                    Err(e) => {
                        println!("Evaluation error: {}\n", e);
                        assert!(false)
                    },

                    Ok(reduced_ast) => {
                        // write tests here
                    }
                }
            }
        }
    }

    #[test]
    fn eval_verify_all_elems_cloned() {
        let test_doc = "".to_string();
        let ft = Rc::new(RefCell::new(FTable::new()));
        let vt = Rc::new(RefCell::new(VTable::new()));

        match lex(test_doc) {
            Err(e) => {
                println!("Lexing error: {}\n", e);
                assert!(false)
            },

            Ok(initial_ast) => {
                match eval(initial_ast.clone(), vt.clone(), ft.clone(), false) {
                    Err(e) => {
                        println!("Evaluation error: {}\n", e);
                        assert!(false)
                    },

                    Ok(reduced_ast) => {
                        // write tests here
                    }
                }
            }
        }
    }*/
}
