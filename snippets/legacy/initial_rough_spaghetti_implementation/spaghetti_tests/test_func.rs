mod func_tests {
    use relish::ast::VTable;
    use relish::ast::{
        func_call, func_declare, lex, Args, ExternalOperation, FTable, Function, Operation,
    };
    use relish::ast::{new_ast, Ast, Ctr, Type};
    use std::cell::RefCell;
    use std::rc::Rc;

    #[test]
    fn decl_and_call_internal_func() {
        let test_internal_func: Function = Function {
            name: String::from("test_func_in"),
            loose_syms: false,
            eval_lazy: false,
            args: Args::Strict(vec![Type::Bool]),
            function: Operation::Internal(Box::new(
                |a: Ast, _b: Rc<RefCell<VTable>>, _c: Rc<RefCell<FTable>>| -> Ctr {
                    let inner = a.borrow();
                    let mut is_bool = false;
                    if let Ctr::Bool(_) = &inner.car {
                        is_bool = true;
                    }

                    Ctr::Bool(is_bool)
                },
            )),
        };
        let ft = Rc::new(RefCell::new(FTable::new()));
        let vt = Rc::new(RefCell::new(VTable::new()));
        let args = new_ast(Ctr::Bool(true), Ctr::None);
        if let Some(s) = func_declare(ft.clone(), Rc::new(RefCell::new(test_internal_func))) {
            print!("Error declaring internal func: {}", s);
            assert!(false);
        }

        let func: Rc<RefCell<Function>>;
        if let Some(f) = ft.borrow().get(&"test_func_in".to_string()) {
            func = f.clone();
        } else {
            print!("failed to retrieve function!");
            assert!(false);
            return;
        }

        if let Ok(ret) = func_call(func, args, vt, ft) {
            match ret {
                Ctr::Bool(b) => assert!(b),
                _ => {
                    print!("invalid return from func!");
                    assert!(false);
                }
            }
        } else {
            print!("call to function failed!");
            assert!(false);
        }
    }

    #[test]
    fn decl_and_call_external_func_singlet() {
        match lex("((input))".to_string()) {
            Err(e) => panic!("{}", e),
            Ok(finner) => {
                let test_external_func: Function = Function {
                    name: String::from("echo"),
                    loose_syms: false,
                    eval_lazy: false,
                    args: Args::Lazy(1),
                    function: Operation::External(ExternalOperation {
                        arg_syms: vec!["input".to_string()],
                        ast: finner,
                    }),
                };
                let ft = Rc::new(RefCell::new(FTable::new()));
                let vt = Rc::new(RefCell::new(VTable::new()));
                let args = new_ast(Ctr::String("test".to_string()), Ctr::None);
                if let Some(s) = func_declare(ft.clone(), Rc::new(RefCell::new(test_external_func)))
                {
                    print!("Error declaring external func: {}", s);
                    assert!(false);
                }

                let func: Rc<RefCell<Function>>;
                if let Some(f) = ft.borrow().get(&"echo".to_string()) {
                    func = f.clone();
                } else {
                    print!("failed to retrieve function!");
                    assert!(false);
                    return;
                }

                match func_call(func, args, vt, ft) {
                    Ok(ret) => match ret {
                        Ctr::String(b) => assert!(b == "test"),
                        _ => {
                            print!("Invalid return from func. Got {:?}\n", ret);
                            assert!(false);
                        }
                    },
                    Err(e) => {
                        print!("Call to function failed: {}\n", e);
                        assert!(false);
                    }
                }
            }
        }
    }

    #[test]
    fn decl_and_call_external_func_multi_body() {
        match lex("((input) (input))".to_string()) {
            Err(e) => panic!("{}", e),
            Ok(finner) => {
                let test_external_func: Function = Function {
                    name: String::from("echo_2"),
                    loose_syms: false,
                    eval_lazy: false,
                    args: Args::Lazy(1),
                    function: Operation::External(ExternalOperation {
                        arg_syms: vec!["input".to_string()],
                        ast: finner,
                    }),
                };
                let ft = Rc::new(RefCell::new(FTable::new()));
                let vt = Rc::new(RefCell::new(VTable::new()));
                let args = new_ast(Ctr::String("test".to_string()), Ctr::None);
                if let Some(s) = func_declare(ft.clone(), Rc::new(RefCell::new(test_external_func)))
                {
                    print!("Error declaring external func: {}", s);
                    assert!(false);
                }

                let func: Rc<RefCell<Function>>;
                if let Some(f) = ft.borrow().get(&"echo_2".to_string()) {
                    func = f.clone();
                } else {
                    print!("failed to retrieve function!");
                    assert!(false);
                    return;
                }

                match func_call(func, args, vt, ft) {
                    Ok(ret) => match ret {
                        Ctr::String(s) => {
                            assert!(s == "test");
                        }
                        _ => {
                            print!("Invalid return from function {:#?}. Should have recieved single string", ret);
                            assert!(false);
                            return;
                        }
                    },
                    Err(e) => {
                        print!("Call to function failed: {}\n", e);
                        assert!(false);
                    }
                }
            }
        }
    }

    #[test]
    fn decl_and_call_func_with_nested_call() {
        let inner_func: Function = Function {
            name: String::from("test_inner"),
            loose_syms: false,
            eval_lazy: false,
            args: Args::Strict(vec![Type::Bool]),
            function: Operation::Internal(Box::new(
                |a: Ast, _b: Rc<RefCell<VTable>>, _c: Rc<RefCell<FTable>>| -> Ctr {
                    let inner = a.borrow();
                    if let Ctr::Bool(b) = &inner.car {
                        if *b {
                            Ctr::String("test".to_string())
                        } else {
                            Ctr::None
                        }
                    } else {
                        Ctr::None
                    }
                },
            )),
        };

        match lex("((test_inner true))".to_string()) {
            Err(e) => panic!("{}", e),
            Ok(finner) => {
                let outer_func: Function = Function {
                    name: String::from("test_outer"),
                    loose_syms: false,
                    eval_lazy: false,
                    args: Args::Lazy(1),
                    function: Operation::External(ExternalOperation {
                        arg_syms: vec!["input".to_string()],
                        ast: finner,
                    }),
                };

                let ft = Rc::new(RefCell::new(FTable::new()));
                let vt = Rc::new(RefCell::new(VTable::new()));
                let args = new_ast(Ctr::Bool(true), Ctr::None);

                if let Some(s) = func_declare(ft.clone(), Rc::new(RefCell::new(inner_func))) {
                    print!("Error declaring inner func: {}", s);
                    assert!(false);
                }
                if let Some(s) = func_declare(ft.clone(), Rc::new(RefCell::new(outer_func))) {
                    print!("Error declaring outer func: {}", s);
                    assert!(false);
                }

                let func: Rc<RefCell<Function>>;
                if let Some(f) = ft.borrow().get(&"test_outer".to_string()) {
                    func = f.clone();
                } else {
                    print!("failed to retrieve function!");
                    assert!(false);
                    return;
                }

                match func_call(func, args, vt, ft) {
                    Ok(ret) => match ret {
                        Ctr::String(b) => assert!(b == "test"),
                        _ => {
                            print!("Invalid return from func. Got {:?}\n", ret);
                            assert!(false);
                        }
                    },
                    Err(e) => {
                        print!("Call to function failed: {}\n", e);
                        assert!(false);
                    }
                }
            }
        }
    }

    /* This test removed because it would make more sense to test this functionality in eval tests
     * this function tested the evaluation of a complex argument ast that could be reduced in eval

    #[test]
    fn decl_and_call_func_eval_arg() {
        let is_true_func: Function = Function{
            name: String::from("is_true?"),
            loose_syms: false,
            eval_lazy: false,
            args: Args::Strict(vec![Type::Bool]),
            function: Operation::Internal(
                |a: Ast, _b: Rc<RefCell<VTable>>, _c: Rc<RefCell<FTable>>| -> Ctr {
                    let inner = a.borrow();
                    if let Ctr::Bool(b) = &inner.car {
                        if *b {
                            Ctr::String("test".to_string())
                        } else {
                            Ctr::None
                        }
                    } else {
                        Ctr::None
                    }
                }
            )
        };

        let echo_func: Function = Function{
            name: String::from("echo"),
            loose_syms: false,
            eval_lazy: false,
            args: Args::Lazy(1),
            function: Operation::External(
                ExternalOperation{
                    arg_syms: vec!["input".to_string()],
                    ast: new_ast(Ctr::Symbol("input".to_string()), Ctr::None)
                }
            )
        };

        let ft = Rc::new(RefCell::new(FTable::new()));
        let vt = Rc::new(RefCell::new(VTable::new()));
        let args = new_ast(
            Ctr::Seg(new_ast(
                Ctr::Symbol("is_true?".to_string()),
                Ctr::Seg(new_ast(
                    Ctr::Bool(true),
                    Ctr::None)))),
            Ctr::None);

        if let Some(s) = func_declare(ft.clone(),
                                      Rc::new(RefCell::new(is_true_func))) {
            print!("Error declaring inner func: {}", s);
            assert!(false);
        }
        if let Some(s) = func_declare(ft.clone(),
                                      Rc::new(RefCell::new(echo_func))) {
            print!("Error declaring outer func: {}", s);
            assert!(false);
        }

        let func: Rc<RefCell<Function>>;
        if let Some(f) = ft.borrow().get(&"echo".to_string()) {
            func = f.clone();
        } else {
            print!("failed to retrieve function!");
            assert!(false);
            return;
        }

        match func_call(func, args, vt, ft) {
            Ok(ret) => {
                match ret {
                    Ctr::String(b) => assert!(b == "test"),
                    _ => {
                        print!("Invalid return from func. Got {:?}\n", ret);
                        assert!(false);
                    }
                }
            },
            Err(e) => {
                print!("Call to function failed: {}\n", e);
                assert!(false);
            }
        }
    }*/

    /*
    // TODO: These tests need completion!
    #[test]
    fn eval_lazy_func_call() {

    }

    #[test]
    fn sym_loose_func_call() {

    }

    #[test]
    fn too_many_args() {

    }

    #[test]
    fn not_enough_args() {

    }

    #[test]
    fn bad_eval_arg() {

    }

    #[test]
    fn bad_eval_fn_body() {

    }*/
}
