/* relish: highly versatile lisp interpreter
 * Copyright (C) 2021 Aidan Hahn
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

mod run;
mod eval;
mod lex;
mod segment;
mod stl;
mod sym;
mod error;

pub mod ast {
    pub use crate::run::run;
    pub use crate::eval::eval;
    pub use crate::lex::lex;
    pub use crate::segment::{Ctr, Seg, Type};
    pub use crate::sym::{Args, SymTable, Symbol, UserFn, ValueType};
    pub use crate::error::{Traceback, start_trace};
}

pub mod stdlib {
    pub use crate::stl::{};
    pub use crate::stl::{
        dynamic_stdlib, static_stdlib,
        load_defaults, load_environment,
        CONSOLE_XDIM_VNAME,
        CONSOLE_YDIM_VNAME,
        POSIX_CFG_VNAME,
        MODENV_CFG_VNAME,
        L_PROMPT_VNAME,
        R_PROMPT_VNAME,
        PROMPT_DELIM_VNAME,
        CFG_FILE_VNAME,
        RELISH_DEFAULT_CONS_HEIGHT,
        RELISH_DEFAULT_CONS_WIDTH,
    };
}

pub mod aux {
    #[cfg(feature="posix")]
    pub use crate::stl::posix::args_from_ast;
    #[cfg(feature="posix")]
    pub use crate::stl::posix::ShellState;
    #[cfg(feature="posix")]
    pub use crate::stl::posix::check_jobs;
}
