/* relish: versatile lisp shell
 * Copyright (C) 2021 Aidan Hahn
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::eval::eval;
use crate::lex::lex;
use crate::error::{Traceback, start_trace};
use crate::segment::{Ctr, Seg};
use crate::sym::SymTable;
use std::path::Path;
use std::fs;
use std::iter::FromIterator;
use std::env::{var, current_dir};

fn get_paths() -> Vec<String> {
    Vec::from_iter(var("PATH")
                   .unwrap_or("".to_string())
                   .split(':')
                   .map(String::from))
}

pub fn find_on_path(filename: String) -> Option<String> {
    let mut prefixes = get_paths();
    if let Ok(s) = current_dir() {
        prefixes.push(String::from(s.to_str().unwrap()));
    }
    prefixes.push(String::from("/"));
    for prefix in prefixes {
        let candidate = Path::new(&prefix.clone()).join(filename.clone());

        if candidate.exists() {
            return Some(String::from(candidate.to_str().unwrap()))
        }
    }
    None
}

pub fn run(filename: String, syms: &mut SymTable) -> Result<(), Traceback> {
    let script_read_res = fs::read_to_string(filename);
    if script_read_res.is_err() {
        Err(start_trace(
            ("<call script>", format!("Couldnt read script: {}", script_read_res.err().unwrap()))
                .into()))
    } else {
        let script_read = script_read_res.unwrap() + ")";
        let script = "(".to_string() + &script_read;
        eval(&*lex(&script)?, syms)?;
        Ok(())
    }
}

pub const RUN_DOCSTRING: &str = "Takes one string argument.
Attempts to find argument in PATH and attempts to call argument";

pub fn run_callback(ast: &Seg, syms: &mut SymTable) ->  Result<Ctr, Traceback> {
    if let Ctr::String(ref filename) = *ast.car {
        if filename.ends_with(".rls") {
            if let Some(filepath) = find_on_path(filename.to_string()) {
                run(filepath, syms)
                    .and(Ok(Ctr::None))
            } else {
                let canonical_path_res = fs::canonicalize(filename);
                if canonical_path_res.is_err() {
                    return Err(start_trace(
                        ("<call script>", canonical_path_res
                         .err()
                         .unwrap()
                         .to_string())
                            .into()))
                }
                let canonical_path = canonical_path_res.ok().unwrap();
                return run(
                    canonical_path
                        .to_string_lossy()
                        .to_string(),
                    syms
                ).and(Ok(Ctr::None))
            }
        } else {
            Err(start_trace(
                ("<call script>", "binary called, unimplemented!")
                    .into()))
        }
    } else {
        Err(start_trace(
            ("<call script>", "impossible: not a string")
            .into()))
    }
}
