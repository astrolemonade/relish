mod bool_lib_tests {
    use relish::ast::{eval, lex, Ctr, SymTable};
    use relish::stdlib::{dynamic_stdlib, static_stdlib};

    #[test]
    fn test_and_true_chain() {
        let document = "(and true true true true true)";
        let result = "true";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_and_true_chain_with_false() {
        let document = "(and true true false true true)";
        let result = "false";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_and_false_chain() {
        let document = "(and false false false false false)";
        let result = "false";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_or_true_chain() {
        let document = "(or true true true true true)";
        let result = "true";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_or_true_chain_with_false() {
        let document = "(or true true false true true)";
        let result = "true";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_or_false_chain() {
        let document = "(or false false false false false)";
        let result = "false";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_not() {
        let document = "(not true)";
        let result = "false";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_toggle_a_bool() {
        let document = "(def tester '' true)";
        let change = "(toggle tester)";
        let check = "(tester)";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        let doc_tree = lex(&document.to_string()).unwrap();
        let change_tree = lex(&change.to_string()).unwrap();
        let check_tree = lex(&check.to_string()).unwrap();

        eval(&doc_tree, &mut syms).unwrap();
        eval(&change_tree, &mut syms).unwrap();

        if let Ctr::Seg(ref s) = *eval(&check_tree, &mut syms).unwrap() {
            if let Ctr::Bool(ref b) = *s.car {
                assert_eq!(false, *b)
            } else {
                panic!()
            }
        } else {
            panic!()
        }

        eval(&change_tree, &mut syms).unwrap();
        if let Ctr::Seg(ref s) = *eval(&check_tree, &mut syms).unwrap() {
            if let Ctr::Bool(ref b) = *s.car {
                assert_eq!(true, *b)
            } else {
                panic!()
            }
        } else {
            panic!()
        }
    }

    #[test]
    fn test_toggle_errors_dont_lose_vars() {
        let document = "(def tester '' 'oops')";
        let change = "(toggle tester)";
        let check = "(tester)";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        let doc_tree = lex(&document.to_string()).unwrap();
        let change_tree = lex(&change.to_string()).unwrap();
        let check_tree = lex(&check.to_string()).unwrap();

        eval(&doc_tree, &mut syms).unwrap();
        if let Err(s) = eval(&change_tree, &mut syms) {
            assert_eq!(
                s.0.first().unwrap().message,
                "can only toggle a boolean".to_string()
            );
            let intermediate = *eval(&check_tree, &mut syms).unwrap();
            if let Ctr::Seg(ref s) = intermediate {
                assert_eq!(s.to_string(), "('oops')".to_string());
            } else {
                eprintln!("did not expect: {}", intermediate);
                panic!()
            }
        } else {
            eprintln!("shouldn't have succeeded!");
            panic!()
        }
    }

    #[test]
    fn test_toggle_errors_dont_lose_funcs() {
        let document = "(def tester '' (oops) oops)";
        let change = "(toggle tester)";
        let check = "(tester '1')";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        let doc_tree = lex(&document.to_string()).unwrap();
        let change_tree = lex(&change.to_string()).unwrap();
        let check_tree = lex(&check.to_string()).unwrap();

        eval(&doc_tree, &mut syms).unwrap();
        if let Err(s) = eval(&change_tree, &mut syms) {
            assert_eq!(
                s.0.first().unwrap().message,
                "cannot toggle a function".to_string()
            );
            if let Ctr::String(ref s) = *eval(&check_tree, &mut syms).unwrap() {
                assert_eq!(*s, "1".to_string());
            } else {
                panic!()
            }
        } else {
            eprintln!("shouldn't have succeeded!");
            panic!()
        }
    }

    #[test]
    fn test_iseq_basic_t() {
        let document = "(eq? true true)";
        let test = lex(&document.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ctr::Bool(b) = *eval(&test, &mut syms).unwrap() {
            assert!(b)
        } else {
            panic!()
        }
    }

    #[test]
    fn test_iseq_basic_f() {
        let document = "(eq? true false)";
        let test = lex(&document.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ctr::Bool(b) = *eval(&test, &mut syms).unwrap() {
            assert!(!b)
        } else {
            panic!()
        }
    }

    #[test]
    fn test_iseq_basic_f_mixed_data() {
        let document = "(eq? true 1)";
        let test = lex(&document.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ctr::Bool(b) = *eval(&test, &mut syms).unwrap() {
            assert!(!b)
        } else {
            panic!()
        }
    }

    #[test]
    fn test_iseq_long_f() {
        let document = "(eq? true true true true true false)";
        let test = lex(&document.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ctr::Bool(b) = *eval(&test, &mut syms).unwrap() {
            assert!(!b)
        } else {
            panic!()
        }
    }

    #[test]
    fn test_iseq_long_t_str() {
        let document = "(eq? '1' '1' '1' '1' '1' '1' '1' '1' '1' '1' '1' '1' '1' '1')";
        let test = lex(&document.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ctr::Bool(b) = *eval(&test, &mut syms).unwrap() {
            assert!(b)
        } else {
            panic!()
        }
    }

    #[test]
    fn test_iseq_t_mixed_numerals() {
        let document = "(eq? 1 1.0)";
        let test = lex(&document.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ctr::Bool(b) = *eval(&test, &mut syms).unwrap() {
            assert!(b)
        } else {
            panic!()
        }
    }

    #[test]
    fn test_iseq_f_wrong_type() {
        let document = "(eq? 1 '1')";
        let test = lex(&document.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ctr::Bool(b) = *eval(&test, &mut syms).unwrap() {
            assert!(!b)
        } else {
            panic!()
        }
    }

    #[test]
    fn test_boolcast_str_t() {
        let document = "(bool 'true')";
        let test = lex(&document.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ctr::Bool(b) = *eval(&test, &mut syms).unwrap() {
            assert!(b)
        } else {
            panic!()
        }
    }

    #[test]
    fn test_boolcast_str_f() {
        let document = "(bool 'false')";
        let test = lex(&document.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ctr::Bool(b) = *eval(&test, &mut syms).unwrap() {
            assert!(!b)
        } else {
            panic!()
        }
    }

    #[test]
    fn test_boolcast_int_t() {
        let document = "(bool 0)";
        let test = lex(&document.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ctr::Bool(b) = *eval(&test, &mut syms).unwrap() {
            assert!(b)
        } else {
            panic!()
        }
    }

    #[test]
    fn test_boolcast_int_f() {
        let document = "(bool 12)";
        let test = lex(&document.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ctr::Bool(b) = *eval(&test, &mut syms).unwrap() {
            assert!(!b)
        } else {
            panic!()
        }
    }

    #[test]
    fn test_boolcast_float_t() {
        let document = "(bool 0.0)";
        let test = lex(&document.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ctr::Bool(b) = *eval(&test, &mut syms).unwrap() {
            assert!(b)
        } else {
            panic!()
        }
    }

    #[test]
    fn test_boolcast_float_f() {
        let document = "(bool 1.2)";
        let test = lex(&document.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ctr::Bool(b) = *eval(&test, &mut syms).unwrap() {
            assert!(!b)
        } else {
            panic!()
        }
    }
}
