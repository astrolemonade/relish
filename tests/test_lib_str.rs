mod str_lib_tests {
    use relish::ast::{eval, lex, SymTable};
    use relish::stdlib::{dynamic_stdlib, static_stdlib};

    #[test]
    fn test_simple_concat() {
        let document = "(concat 'test')";
        let result = "'test'";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_poly_concat() {
        let document = "(concat 'test' 1 2 3)";
        let result = "'test123'";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_empty_concat() {
        let document = "(concat)";
        let result = "''";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_strlen_str() {
        let document = "(strlen 'test')";
        let result = 4;
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_strlen_int() {
        let document = "(strlen 1000)";
        let result = 4;
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_strlen_float() {
        let document = "(strlen 10.2)";
        let result = 4;
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_strlen_seg() {
        let document = "(strlen (1 2 3))";
        let result = 7;
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_strlen_bool() {
        let document = "(strlen true)";
        let result = 4;
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_strcast_i() {
        let document = "(string 4)";
        let result = "'4'";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_strcast_seg() {
        let document = "(string (1 2 3))";
        let result = "'(1 2 3)'";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_contains() {
        let document = "(substr? 'bigger' 'ger')";
        let result = "true";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_doesnt_contain() {
        let document = "(substr? 'smaller' 'ger')";
        let result = "false";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_split() {
        let document = "(split 'one.two.three' '.')";
        let result = "('one' 'two' 'three')";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_split_big_delim() {
        let document = "(split 'one:d:two:d:three' ':d:')";
        let result = "('one' 'two' 'three')";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_splitnt() {
        let document = "(split 'one.two.three' '-')";
        let result = "('one.two.three')";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }
}
